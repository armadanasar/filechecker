#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SAFE_CRASH printf("This program has crashed, exiting for safety.. Bye..\n");\
                   getch();\
                   exit(18);

#define NEWLINE printf("\n");
#define RETURN_TO_MAIN_MENU main(1);
#define END_EXERCISE getch();
#define CLEAR_SCREEN system("cls");

#define STANDARD_CLEANUP  NEWLINE\
                          END_EXERCISE\
                          RETURN_TO_MAIN_MENU

#define GRADE_STRING "Your grade is %c and your score is %.2lf"

int main()
{
    int questionNumbers = 10;

    int questionFilesPresent = 0;
    int answerKeyFilePresent = 0;
    int iteration;

    const char questionFileName[BUFSIZ];
    const char answerKeyFileName[] = "f:\\answer_key.txt";
    char temporaryFileName[BUFSIZ];
    char txtTail[] = ".txt";

    for (iteration = 0; iteration < questionNumbers; iteration++)
    {
        snprintf(temporaryFileName, sizeof(temporaryFileName), "%s%d%s", "f:\\question_no_", iteration, txtTail);
        printf("%s\n", temporaryFileName);

        FILE *questionFile;
        if (questionFile = fopen(temporaryFileName, "r"))
        {
            fclose(questionFile);
        }

        else
        {
            SAFE_CRASH
        }
    }

    questionFilesPresent = 1;

    FILE *answerKeyFile;
    if (answerKeyFile = fopen(answerKeyFileName, "r"))
    {
        answerKeyFilePresent = 1;
        fclose(answerKeyFile);
    }

    else
    {
        answerKeyFilePresent = 0;
    }

    if ((questionFilesPresent == 1) && (answerKeyFilePresent == 1))
    {
        return 1;
    }

    else
    {
        return 0;
    }

    return 0;
}
